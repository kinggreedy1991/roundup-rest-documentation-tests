/*global
    console, window
*/
//========================== CONFIG ========================= //
var HTTP_PATH = 'http://admin:admin@localhost:9999/python-dev';


//========================== TESTS ========================== //
/*
   FORMAT:
    test_num: A Test, with smaller steps inside
    test_num() {
        window.test_num_step = function () {
            define step inside
        }
    }

    testVars[name]: variable of the test. Global, so other tests can read it

   DOC:
    function testXHRDispatch(builder, callback)
        Description: create XHR Object, fill out the needed step and fire it

        Args:
            builder(function) Add method, url, headers to xhr
                              RETURN form data
            callback(function) Handle the result

        Returns:
            Nothing
*/
/*======= 0 ========*/
function test_0() {
    //Step 0: just read the issues
    window.test_0_0 = function () {
        testXHRDispatch(function (xhr) {
            xhr.open('GET', HTTP_PATH + '/rest/issue', true);
            return null;
        }, function (xhr) {
            console.assert(xhr.status === 200, "GET status is:", xhr.status, "response:", xhr.responseText);
        });
    };
}

/* ======= 1 ======== */
function test_1() {
    //Step 0: read the numbers of the issues
    window.test_1_0 = function () {
        testXHRDispatch(function (xhr) {
            xhr.open('GET', HTTP_PATH + '/rest/issue', true);
            return null;
        }, function (xhr) {
            console.assert(xhr.status === 200, "GET status is:", xhr.status, "response:", xhr.responseText);
            if (xhr.status === 200) {
                var obj = JSON.parse(xhr.responseText);
                testVars.total_issues = obj.data.length;
            }
        });
    };

    //Step 1: post a new object
    window.test_1_1 = function () {
        testXHRDispatch(function (xhr) {
            xhr.open('POST', HTTP_PATH + '/rest/issue', true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            var formdata = "title=apitest&status=2";
            return formdata;
        }, function (xhr) {
            console.assert(xhr.status === 201, "POST status is:", xhr.status, "response:", xhr.responseText);
            if (xhr.status === 201) { //TODO: Temporally fix to pass tests, actually status code is 201
                var obj = JSON.parse(xhr.responseText);
                testVars.obj = obj.data;
            }
        });
    };

    //Step 2: read all the issues again
    window.test_1_2 = function () {
        testXHRDispatch(function (xhr) {
            xhr.open('GET', HTTP_PATH + '/rest/issue', true);
            return null;
        }, function (xhr) {
            console.assert(xhr.status === 200, "GET status is", xhr.status, "response:", xhr.responseText);
            if (xhr.status === 200) {
                var obj = JSON.parse(xhr.responseText);
                console.assert(obj.data.length === testVars.total_issues + 1, "PREV", testVars.total_issues, ", NOW", obj.data.length);

                var found = false, i;
                for (i = 0; i < obj.data.length; i += 1) {
                    if ((obj.data[i].id === testVars.obj.id) && (obj.data[i].link === testVars.obj.link)) {
                        found = true;
                        break;
                    }
                }
                console.assert(found, "Created object not found in the collection");
            }
        });
    };

    //Step 3: verify the issue
    window.test_1_3 = function () {
        testXHRDispatch(function (xhr) {
            xhr.open('GET', testVars.obj.link, true);
            return null;
        }, function (xhr) {
            console.assert(xhr.status === 200, "GET status is:", xhr.status, "response:", xhr.responseText);
            if (xhr.status === 200) {
                var obj = JSON.parse(xhr.responseText);
                console.assert(obj.data.id === testVars.obj.id, "ID does not match");
                console.assert(obj.data.attributes.title === "apitest", "Incorrect Title: ", obj.data.title);
                console.assert(obj.data.attributes.status === "2", "Incorrect Status: ", obj.data.status);
            }
        });
    };

    //Step 4: delete the issue
    window.test_1_4 = function () {
        testXHRDispatch(function (xhr) {
            xhr.open('DELETE', testVars.obj.link, true);
            return "a=a"; //bug in the code
        }, function (xhr) {
            console.assert(xhr.status === 403, "GET status is:", xhr.status, "response:", xhr.responseText); // it's forbidden
        });
    };

    //Step 5: verify if the issue is deleted (not now)
    window.test_1_5 = function () {
        testXHRDispatch(function (xhr) {
            xhr.open('GET', testVars.obj.link, true);
            return null;
        }, function (xhr) {
            console.assert(xhr.status === 200, "GET status is:", xhr.status, "response:", xhr.responseText);
            //TODO: Finish delete should be working, or something
        });
    };
}

/* ======= 2 ======== */
function test_2() {
    //Step 0: post a message
    window.test_2_0 = function () {
        testXHRDispatch(function (xhr) {
            xhr.open('POST', HTTP_PATH + '/rest/msg', true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            var formdata = "content=This is API test message";
            return formdata;
        }, function (xhr) {
            console.assert(xhr.status === 201, "POST status is:", xhr.status, "response:", xhr.responseText);
            if (xhr.status === 201) {
                var obj = JSON.parse(xhr.responseText);
                testVars.msg_obj = obj.data;
            }
        });
    };

    //Step 1: read it
    window.test_2_1 = function () {
        testXHRDispatch(function (xhr) {
            xhr.open('GET', testVars.msg_obj.link, true);
            return null;
        }, function (xhr) {
            console.assert(xhr.status === 200, "GET status is:", xhr.status, "response:", xhr.responseText);
            if (xhr.status === 200) {
                var obj = JSON.parse(xhr.responseText);
                console.assert(obj.data.attributes.content === "This is API test message", "Incorrect Content: ", obj.data.attributes.content);
            }
        });
    };

    //Step 2: try PUT
    window.test_2_2 = function () {
        testXHRDispatch(function (xhr) {
            xhr.open('PUT', testVars.msg_obj.link, true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            var formdata = "content=This is another API test message";
            return formdata;
        }, function (xhr) {
            console.assert(xhr.status === 200, "PUT status is:", xhr.status, "response:", xhr.responseText);
        });
    };

    //Step 3: read it again
    window.test_2_3 = function () {
        testXHRDispatch(function (xhr) {
            xhr.open('GET', testVars.msg_obj.link, true);
            return null;
        }, function (xhr) {
            console.assert(xhr.status === 200, "GET status is:", xhr.status, "response:", xhr.responseText);
            if (xhr.status === 200) {
                var obj = JSON.parse(xhr.responseText);
                console.assert(obj.data.attributes.content === "This is another API test message", "Incorrect Content: ", obj.data.attributes.content);
            }
        });
    };
}

/*======= 3 ========*/
function test_3() {
    //Step 0: PATCH to Collection is now allowed
    window.test_3_0 = function () {
        testXHRDispatch(function (xhr) {
            xhr.open('PATCH', HTTP_PATH + '/rest/issue', true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            return "";
        }, function (xhr) {
            console.assert(xhr.status === 405, "PATCH status is:", xhr.status, "response:", xhr.responseText);
        });
    };
}

/*======= 4 ========*/
function test_4() {
    //Step 0: PUT to Collection is now allowed
    window.test_4_0 = function () {
        testXHRDispatch(function (xhr) {
            xhr.open('PUT', HTTP_PATH + '/rest/issue', true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            return "";
        }, function (xhr) {
            console.assert(xhr.status === 405, "PUT status is:", xhr.status, "response:", xhr.responseText);
        });
    };
}

/*======= 5 ========*/
function test_5() {
    //Step 0: Get random issue
    window.test_5_0 = function () {
        testXHRDispatch(function (xhr) {
            xhr.open('GET', HTTP_PATH + '/rest/issue', true);
            return null;
        }, function (xhr) {
            console.assert(xhr.status === 200, "GET status is:", xhr.status, "response:", xhr.responseText);
            if (xhr.status === 200) {
                var obj = JSON.parse(xhr.responseText);
                testVars.obj_link = obj.data[0].link;
            }
        });
    };

    //Step 1: POST to Element is now allowed
    window.test_5_1 = function () {
        testXHRDispatch(function (xhr) {
            xhr.open('POST', testVars.obj_link, true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            return "";
        }, function (xhr) {
            console.assert(xhr.status === 405, "POST status is:", xhr.status, "response:", xhr.responseText);
        });
    };
}

/*********** PUT TEST ABOVE AND DON'T TOUCH BELOW **************/
var testNum = -1;
var testStep = -1;
var testVars = {};
var totalTest = 0;
var totalTask = 0;
var totalError = 0;

function testXHRDispatch(builder, callback) {  //wrapper
    var xhr = new XMLHttpRequest();
    var formdata = builder(xhr);

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            callback(xhr);
            testController();
        }
    };

    xhr.send(formdata);
}

function testController() { //call all the tests until nothing is left
    var callingTest;

    testStep += 1;
    callingTest = window['test_' + testNum + '_' + testStep];
    if (callingTest !== undefined) {
        if (testStep !== 0) {
            console.groupEnd();
        }
        console.group('%c STEP %d', 'background: #0074D9; color: white', testStep);
        totalTask += 1;

        callingTest();
        return;
    }

    if (testStep > 0) {
        console.groupEnd(); // end any test step if available
    }

    testNum += 1;
    testStep = -1;
    callingTest = window['test_' + testNum];
    if (callingTest !== undefined) {
        console.groupEnd(); //end the previous test
        console.group('%c TEST %d', 'background: #222; color: #bafa55', testNum);
        totalTest += 1;
        totalTask += 1;

        callingTest();
        testController();
        return;
    }

    console.groupEnd();

    if (totalError === 0) {
        console.log('%c COMPLETED: %d tests, %d tasks', 'background: #222; color: #bafa55', totalTest, totalTask);
    } else {
        console.log('%c FAILED: %d tests, %d tasks, %d errors', 'background: #9c2b2e; color: white', totalTest, totalTask, totalError);
    }
    return;
}

function main() {
    //console.log('%c BEGIN', 'background: #222; color: #bafa55');
    testController();
}

main();

/*
window.console_assert_old = console.assert;
console.assert = function (statement) {
    totalError += !statement;
    window.console_assert_old.apply(this, arguments);
}
*/
