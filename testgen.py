import argparse
import xmlrpclib
import random

field_dict = {
    'titles': [
        'This is a very longggggggggggggggggggggg issue title {}',
        'Short issue title {}',
        'Ramdom.title {}'
    ],
    'statuses': [1, 2, 3],
    'keywords': ['26backport', '64bit', 'easy', 'needs review', 'patch']
}

def main(args):
    roundup_server = xmlrpclib.ServerProxy(
        'http://{}:{}@localhost:9999/python-dev/xmlrpc'.format(
            args.username,
            args.password
        ),
        allow_none=True
    )
    number = args.number or 10
    for i in range(number):
        roundup_server.create(
            'issue', 
            'title={}'.format(random.choice(field_dict['titles']).format(i)),
            'status={}'.format(random.choice(field_dict['statuses'])),
            'keywords={}'.format(random.choice(field_dict['keywords']))
        )
    print('Done')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--user', dest='username', required=True)
    parser.add_argument('-p', '--pass', dest='password', required=True)
    parser.add_argument('-n', '--num', dest='number', type=int)
    args = parser.parse_args()
    main(args)