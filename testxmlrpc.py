import argparse
import xmlrpclib
import random


def main(args):
    roundup_server = xmlrpclib.ServerProxy(
        'http://{}:{}@localhost:9999/python-dev/xmlrpc'.format(
            'admin',
            'admin'
        ),
        allow_none=True
    )
    print roundup_server.display('msg2')
    print('Done')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # parser.add_argument('-u', '--user', dest='username', required=True)
    # parser.add_argument('-p', '--pass', dest='password', required=True)
    args = parser.parse_args()
    main(args)
